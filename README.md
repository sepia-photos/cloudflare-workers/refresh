# refresh

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by Cloudflare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker handles requests with the /refresh endpoint. It accepts a refresh token sent as a cookie in the form of a uuid which is passed on to the "refresh" IBM cloud function. The IBM cloud function returns a new short-lived access token (in the form of a JWT) which the Cloudflare worker sets as a cookie.

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
